<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Info collection</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="col-sm-3"></div>
<div class="col-sm-6">
<form class="form-horizontal" method="post" action="process.php">

    <div class="form-group">
        <label for="firstName" class="col-sm-2 control-label">First Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name">
        </div>
    </div>

    <div class="form-group">
        <label for="lastName" class="col-sm-2 control-label">Last Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name">
        </div>
    </div>

    <div class="form-group">
        <label for="ID" class="col-sm-2 control-label">Id Number</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="ID" name="ID" placeholder="ID Number">
        </div>
    </div>

    <div class="form-group">
        <label for="bloodGroup" class="col-sm-2 control-label">Blood Group</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="bloodGroup" name="bloodGroup" placeholder="Blood Group">
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Submit</button>
            </div>
    </div>
    <div class="col-sm-3"></div>
</form>
</div>
</body>
</html>